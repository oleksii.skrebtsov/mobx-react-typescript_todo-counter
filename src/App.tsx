import React, { Component } from "react";
import "./App.css";
import { observer } from "mobx-react";

import Counter from "./components/counter";
import Todo from "./components/todo";

@observer
class App extends Component {
  render() {
    return (
      <div className='App'>
        <Counter />
        <Todo/>
      </div>
    );
  }
}

export default App;
