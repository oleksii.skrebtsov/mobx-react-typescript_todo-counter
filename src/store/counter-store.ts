import { observable, makeObservable, action } from 'mobx';
class Counter {
    count=0;

    constructor() {
        makeObservable(this, {
            count:observable,
            handleIncrement:action,
            handleDecrement:action,
        });
      }
    
    handleIncrement = () => {
        this.count++
    }
    handleDecrement = () => {
        if(this.count!==0) {
            this.count--
          }
    }
}

export default new Counter();