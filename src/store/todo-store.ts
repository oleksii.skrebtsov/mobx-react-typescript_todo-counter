import { action, makeObservable, observable } from "mobx";
import { todoType } from "../types/app-types";

import counterStore from "../store/counter-store"

class Todo {
  todos: todoType[] = [];
  id = 1;
  inputValue = "";

  constructor() {
    makeObservable(this, {
      todos: observable,
      inputValue: observable,
      changeValue: action,
      addItem: action,
      deleteItem: action,
      combineValues: action,
    });
  }
  changeValue = (value: string) => {
    this.inputValue = value;
  };
  addItem = (enter: React.KeyboardEvent<HTMLInputElement>) => {
    if (enter.key === "Enter" && this.inputValue) {
      const todo: todoType = {
        id: this.id++,
        title: this.inputValue,
      };
      this.todos.push(todo);
      this.changeValue("");
    }
  };
  deleteItem = (el: todoType) => {
    const elementIndex = this.todos.indexOf(el);
    this.todos.splice(elementIndex, 1);
  };
  combineValues = (todo: todoType) => {
    const foundElement = this.todos.find((el) => el.id === todo.id);
    if (foundElement) {
      foundElement.title = counterStore.count;
    }
  };
}

export default new Todo();
