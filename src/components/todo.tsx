import { observer } from "mobx-react";
import React, { Component } from "react";

import todoStore from "../store/todo-store";
import { todoType } from "../types/app-types";

@observer
class Todo extends Component {
  render() {
    return (
      <div className='todo'>
        <input
          value={todoStore.inputValue}
          onChange={(e) => todoStore.changeValue(e.target.value)}
          onKeyPress={(enter) => todoStore.addItem(enter)}
        />
        {todoStore.todos.map((todoElem: todoType) => {
          return (
            <div key={todoElem.id}>
              <h2>{todoElem.title}</h2>
              <button onClick={() => todoStore.deleteItem(todoElem)}>delete</button>
              <button onClick={() => todoStore.combineValues(todoElem)}>
                Combine value with Counter
              </button>
            </div>
          );
        })}
      </div>
    );
  }
}
export default Todo;
