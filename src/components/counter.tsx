import React, { Component } from "react";
import { observer } from "mobx-react";

import counterStore from "../store/counter-store";

@observer class Counter extends Component {

  render() {
    return (
      <div className='counter'>
        <h1>{counterStore.count}</h1>
        <button onClick={counterStore.handleIncrement}>+</button>
        <button onClick={counterStore.handleDecrement}>-</button>
      </div>
    );
  }
}

export default Counter;
